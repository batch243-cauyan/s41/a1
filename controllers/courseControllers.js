const Course = require ('../models/courses');
const User = require('../models/users');
const router = require('../routes/userRoutes');
// const mongoose = require('mongoose')
const bcrypt = require('bcrypt');

const auth = require('../auth');
const { request } = require('express');




// retrieve all active courses
module.exports.getAllActive = async (req,res)=>{

    await Course.find({isActive:true}).sort({name:1})
    .then(result=>
        {return res.status(200).send(result)}
        )
    .catch(error=>{
        res.status(400).send(error)
    })
}


/*
    Steps:
        1.Create a new Course object using mongoose model and information from the request of the user.
        2.Save the new Course the database. 

*/

module.exports.addCourse = (req,res)=>{
    const userData = auth.decode(req.headers.authorization);
    
    console.log(userData)

    let newCourse = new Course ({

        name: req.body.name,
        description: req.body.description,
        price: req.body.price,
        slots: req.body.slots
        
        })
  
    if (userData.isAdmin){
        return newCourse.save()
        .then(course=>{
            console.log(course)
            res.status(201).send(true)
        })
        .catch(err=>{
            // console.log(err)
            res.status(400).send(err);
        })
     }
    else{
        return res.send(`You don't have admin privileges`)
    }
}


// retrieiving a specific course 

module.exports.getCourse = async (req,res)=>{
    const courseId = req.params.courseId


    await Course.findById(courseId)
    .then(result=>
        {return res.status(200).send(result)}
        )
    .catch(error=>{
        // error.message = "Invalid ID"
        res.status(400).send(error.message)
    })
}


// update specified course

module.exports.updateCourse = async (req,res) =>{
    const token = req.headers.authorization;
    const userData = auth.decode(token);
    const courseId = req.params.courseId

    console.log(userData);
    let updatedCourse= {
        name:req.body.name,
        description: req.body.description,
        price: req.body.price,
        slots: req.body.slots
    }


    if (userData.isAdmin){
        return Course.findByIdAndUpdate(courseId, updatedCourse, {new:true})
        .then(result=>{
            res.send(result)
        })
        .catch(err=>{
            res.send(err)
        })
    }
    else{
        return res.send("You don't have access to this page!")
    }


}



module.exports.archiveCourse = async (req,res) =>{
    const token = req.headers.authorization;
    const userData = auth.decode(token);
    const courseId = req.params.courseId


    console.log(userData);
    let archivedCourse= {
        isActive : req.body.isActive
    }


    if (userData.isAdmin){
        return Course.findByIdAndUpdate(courseId, archivedCourse, {new:true})
        .then(result=>{
            console.log(result)
            res.json({isActive: result.isActive})
        })
        .catch(err=>{
            res.send(err)
        })
    }
    else{
        return res.send("You don't have access to this page!")
    }

}


module.exports.getAllArchived = async (req,res)=>{
    const token = req.headers.authorization;
    const userData = auth.decode(token);

    if (userData.isAdmin){
        await Course.find({isActive:false}).sort({name:1})
        .then(result=>
            {return res.status(200).send(result)}
        )
        .catch(error=>{
            res.status(400).send(error)
         })
        }
        else{
            return res.status(400).json({message: "You don't have 'admin' privileges."})
        }
    }


    module.exports.getAllCourses = async (req,res)=>{
        const token = req.headers.authorization;
        const userData = auth.decode(token);
    
        if (userData.isAdmin){
            await Course.find({}).sort({name:1})
            .then(result=>
                {return res.status(200).send(result)}
            )
            .catch(error=>{
                res.status(400).send(error)
             })
            }
            else{
                return res.status(400).json({message: "You don't have 'admin' privileges."})
            }
        }