const Course = require ('../models/courses.js');
const User = require('../models/users');
const router = require('../routes/userRoutes');
// const mongoose = require('mongoose')
const bcrypt = require('bcrypt');

const auth = require('../auth');
const { response } = require('express');
// Check if email already exists
/*
    Steps:
    1. Use mongoose "find" method to find duplicate emails.
    2. Use the "then" mthod to send a response to the frontend application based on the result of the find method.

*/


module.exports.checkEmailExists = (req, res, next)=>{
    // The result is sent back to the frontend via the then method
    return User.find({email:req.body.email}).then(result=>{
        let message = "";
            // find method returns an array record of matching documents
        if(result.length > 0){
            message = `The ${req.body.email} is already taken, please use other email.`
            return res.status(200).send(message)
        }
            // No duplicate email found
            // The email is not yet registered in the database.
        else{
            // message = `That the email: ${req.body.email} is not yet taken.`
            // return res.status(200).send(message)
             next();
        }
    })
}


module.exports.registerUser =  (req,res) =>{
    // creates varaible "newUser" and instantiates a new 'User' object using mongoose model
      let newUser = new User ({
        firstName: req.body.firstName,
        lastName : req.body.lastName,
        email : req.body.email,
        // password:req.body.password,
        // salt - salt rounds that bcrypt algorithm will run to encrypt the password
        password: bcrypt.hashSync (req.body.password, 10),
        mobileNo: req.body.mobileNo
      })

      return  newUser.save()
      .then(user=>res.status(201).send(`Congratulations, Sir/Ma'am ${newUser.firstName}!. You are now registered.`))
      .catch(error=>{
        console.log(error);
        res.status(400).send(`Sorry ${newUser.firstName}, there was an error during the registration. Please Try again!`)
      })

 
}


module.exports.getAllUser = async (req,res)=>{

    await User.find({}).sort({lastName:1})
    .then(result=>
        {return res.status(200).send(result)}
        )
    .catch(error=>{
        res.status(204).send(`No users found.`)
    })
}

// User Authentication  
/*
    Steps:
        1. Check database if the user email exists.
        2. compare the password provided in the login form with the password stored in the database.


*/


// Start of LOGIN 
module.exports.loginUser = (req,res) =>{
    // The findOne method, returns the first record in the collection that matches the search criteria.
    
    // if(!req.body.email === null) 
    return User.findOne({email:req.body.email})
    .then((user => {
        if (user === null){
            return res.send(`Your email: ${req.body.email} is not yet registered. Register first!`) 
        }
        else{
            //  compares db hashed/encrypted password with the entered password of the user
            // The compareSync method is used to compare a non encrypted password from the login to the encrypted password retrieved. It will return true or false value depending on the result
            const isPasswordCorrect = bcrypt.compareSync(req.body.password, user.password);

            if(isPasswordCorrect){
                return res.send({accessToken: auth.createAccessToken(user)})
                // return res.send(`Logged in successfully!`)
            }

                return res.send(`Incorrect password, please try again!`)
        }

    }))

}
// END of LOGIN

//
module.exports.findUser = async (req,res)=>{

    await User.findById({_id: req.body.id})//expecting an id input from the user 
    .then(result=>        
        {
            //the result document's password property is set to an empty string to abstract the password information.
            result.password = "";
            
            return res.status(200).send(result)}
        )
    .catch(error=>{
        console.log(error)
        res.status(400).send(`No users found.`)
    })
}



module.exports.profileDetails = (req,res) =>{
    // user will be object that contains the id and email of the user that is currently logged in.
    const userData = auth.decode(req.headers.authorization);
    console.log(userData);
    return User.findById(userData.id).then(result=>{
        result.password = "Confidential";
        return res.send(result)
    }).catch(err => {
        return res.send(err);
    })
}


module.exports.updateRole = async (req,res)=>{
    const token = req.headers.authorization;
    const userData = auth.decode(token);

    let idToBeUdated = req.params.userId

    // let adminStatus = {
    //     isAdmin: req.params.isAdmin
    // }

    if (userData.isAdmin){

        await User.findById(idToBeUdated)
          .then(result=>{
                let update = !result.isAdmin;
            
            return User.findByIdAndUpdate(idToBeUdated, {isAdmin : update}, {new:true})
                    .then(document => {
                        document.password = "Confidential";
                        res.status(200).send(document)})
                    .catch(err => res.send(err))
          })
          .catch(err => res.send(err))

    }
    else{
        return res.send("You don't have access on this page!")
    }

}

// Enrollment

module.exports.enroll = async (req,res) =>{

    const token = req.headers.authorization;
    let userData = auth.decode(token);

    if (!userData.isAdmin){
        let courseId = req.params.courseId;

        let data = {
            courseId: courseId,
            userId: userData.id
        }

        let isCourseUpdated = await Course.findById(data.courseId).then(course=>{

            course.enrollees.push({userID: data.userId});
            course.slots -= 1;

            return course.save().then(() => {
                return true;
            }).catch(err => {console.log(err); return false})
          
        })  .catch( err => {
            console.log(err);
            return res.send(false);
         });


        let isUserUpdated = await User.findById(data.userId)
                            .then(result=>{
                                result.enrollments.push({courseId: data.courseId});
                                
                                return result.save()
                                .then(success =>{ return true; })
                                .catch(err => {console.log(err); return false;})

                            }).catch(err => {return res.send(false)});

        (isCourseUpdated && isUserUpdated) ? res.send("You are now enrolled.") : res.send("We encountered an error in your enrollment, please try again!")

    } //end of IF

    else{

        return res.status(400).send("You are admin, you cannot enroll to course.")

    }


}