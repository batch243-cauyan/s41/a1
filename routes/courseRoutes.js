const express = require('express');

const router = express.Router();
const courseController = require('../controllers/courseControllers');
const auth = require('../auth')


// endpoint
router.post('/', auth.verify, courseController.addCourse);

// endpoint for getting all active courses
router.get('/allCourses',auth.verify, courseController.getAllCourses);

router.get('/allActiveCourses',courseController.getAllActive);


// router.get('/archivedCourses', courseController.getAllArchived);
router.get('/archivedCourses', auth.verify, courseController.getAllArchived);

// endpoint for getting a specific course
router.get('/:courseId', courseController.getCourse);


// endpoint update a specific course
router.put('/update/:courseId', auth.verify, courseController.updateCourse);

// endpoint archive a course
router.patch('/:courseId/archive', auth.verify, courseController.archiveCourse)



module.exports = router


